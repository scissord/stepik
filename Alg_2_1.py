def fib(n):
    x, y = 0, 1
    for i in range(n):
        x , y =  x + y, x
    return x


def main():
    n = int(input())
    print(fib(n))


if __name__ == "__main__":
    main()