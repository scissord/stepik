def fib_digit(n):
    x, y = 0, 1
    for i in range(n):
        x, y = (x + y)%10, x
    return x


def main():
    n = int(input())
    print(fib_digit(n))


if __name__ == "__main__":
    main()