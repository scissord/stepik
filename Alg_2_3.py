def fib_mod(n, m):
    a, b = 0, 1
    cache = []
    for i in range(n):
        a, b = (a + b) % m, a
        cache.append(a)
        if a == 0 and b == 1:
            break
    return cache[(n%(len(cache))) -1]

def main():
    n, m = map(int, input().split())
    print(fib_mod(n, m))
    print(identity_matrix(int(input())))

if __name__ == "__main__":
    main()