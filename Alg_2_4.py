def gcd(a, b):
    while a:
        a, b = b % a, a
    return b



def main():
    a, b = map(int, input().split())
    print(gcd(a, b))


if __name__ == "__main__":
    main()