n, volume = map(int, input().split())
items = sorted([tuple(map(int,input().split())) for _ in range(n)], key = lambda x: x[0] / x[1])
sum = 0
for av_price, vol in items[::-1]:
    if volume > vol:
        volume -= vol
        sum += vol * av_price
    else:
        vol -= volume
        sum += volume * av_price
        break
print(sum)