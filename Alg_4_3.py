s = input()
h = {}
for l in s:
    try:
        h[l] += 1
    except:
        h[l] = 1
h = [[i[0], i[1], None, None] for i in map(list, h.items())]
pop_min_leave = lambda h: h.pop(h.index(min(h, key=lambda el: el[1])))
for i in range(len(h) - 1):
    node1 = pop_min_leave(h)
    node2 = pop_min_leave(h)
    h.append([None, node1[1] + node2[1], node1, node2])
letters_code = {}
def get_all_code(node, code=''):
    if node[0] is None:
        if node[2] is not None:
            get_all_code(node[2], code + '0')
        if node[3] is not None:
            get_all_code(node[3], code + '1')
    else:
        letters_code[node[0]] = code if len(code) > 0 else '0'
get_all_code(h[0])
code_str = ''
for i in s:
    code_str += letters_code[i]
print(str(len(letters_code)) + ' ' + str(len(code_str)))
print('\n'.join(['{0}: {1}'.format(*i) for i in letters_code.items()]))
print(code_str)

