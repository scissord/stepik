letters_count, code_length = map(int,input().split())
code_d = {}
for i in range(letters_count):
    letter, code = input().split(': ')
    code_d[code] = letter
s = input()
while len(s) > 0:
    co = ''
    while co not in code_d:
        co += s[0]
        s = s[1:]
    print(code_d[co], end='')
