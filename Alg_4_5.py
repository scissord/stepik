class double_tree:
    def __init__(self):
        self.dTree = []

    def insert(self, x):
        self.dTree.append(x)
        self._siftUp()

    def extractMax(self):
        if len(self.dTree) > 0:
            max = self.dTree[0]
            if len(self.dTree) > 1:
                self.dTree[0] = self.dTree.pop()
                self._siftDown()
            else:
                self.dTree.pop()
            return max
        return None

    def _siftDown(self):
        i = 1
        while True:
            checkIndex = 0
            if i * 2 + 1 <= len(self.dTree):
                if self.dTree[i * 2 - 1] >= self.dTree[i * 2]:
                    checkIndex = i * 2
                else:
                    checkIndex = i * 2 + 1
            elif i * 2 <= len(self.dTree):
                checkIndex = i * 2
            else:
                break

            if self.dTree[i - 1] < self.dTree[checkIndex - 1]:
                self.dTree[i - 1], self.dTree[checkIndex - 1] = self.dTree[checkIndex - 1], self.dTree[i - 1]
                i = checkIndex
                continue
            else:
                break

    def _siftUp(self):
        i = len(self.dTree)
        while i != 1:
            parentIndex = i // 2
            if self.dTree[i - 1] > self.dTree[parentIndex - 1]:
                self.dTree[i - 1], self.dTree[parentIndex - 1] = self.dTree[parentIndex - 1], self.dTree[i - 1]
                i = parentIndex
            else:
                break


prioretyQuee = double_tree()
for i in range(int(input())):
    arg = input().split(' ')
    if len(arg) > 1:
        prioretyQuee.insert(int(arg[1]))
    else:
        print(prioretyQuee.extractMax())
