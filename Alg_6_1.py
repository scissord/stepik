def doubleSearch(obj, x):
    l, r = 0, len(obj) - 1
    while l <= r:
        m = int((l + r) / 2)
        if obj[m] > x:
            r = m - 1
        elif obj[m] < x:
            l = m + 1
        else:
            return m + 1
    return -1

arr = list(map(int, input().split()))[1:]
needFind = list(map(int, input().split()))[1:]
print(*map(lambda x: doubleSearch(arr, x), needFind))
