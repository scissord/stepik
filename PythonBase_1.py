scope = {'global':[None]}

def createNS(namespace, parent):
    global scope
    if parent in scope:
        scope[namespace] = [parent]
        return namespace
    return None

def addVar(namespace, var):
    global scope
    scope[namespace].append(var)

def getNS(namespace, var):
    if namespace:
        if var in scope[namespace]:
            return namespace
        else:
            return getNS(scope[namespace][0],var)
    else:
        return None

for n in range(int(input())):
    command = input().split()
    if command[0] == 'create':
        createNS(*command[1:3])
    elif command[0] == 'add':
        addVar(*command[1:3])
    elif command[0] == 'get':
        print(getNS(*command[1:3]))