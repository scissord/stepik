import time


class Loggable:
    def log(self, msg):
        print(str(time.ctime()) + ': ' + str(msg))


class LoggableList(list, Loggable):
    def append(self, p_object):
        super(LoggableList, self).append(p_object)
        self.log(p_object)

loggable = LoggableList()
for i in range(10):
    loggable.append(i)
print(loggable)