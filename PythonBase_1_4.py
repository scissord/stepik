class NonPositiveError(BaseException):
    pass

class PositiveList(list):
    def append(self, p_object):
        if p_object > 0:
            super(PositiveList, self).append(p_object)
        else:
            raise NonPositiveError

print(__name__)
if __name__ == '__main__':
    pos = PositiveList()
    pos.append(1)
    print(pos)
    pos.append(-1)
