import re, requests
a = input()
b = input()
pattern = r'<a.*?href=[\'|\"](.*?)[\'|\"]>'
doc = requests.get(a)
answer = 'No'
if doc.status_code == 200 and 'html' in doc.headers['Content-type']:
    urls = re.findall(pattern, doc.text)
    for url in urls:
        doc2 = requests.get(url)
        if doc2.status_code == 200 and 'html' in doc2.headers['Content-type']:
            urls2 = re.findall(pattern, doc2.text)
            if b in urls2:
                answer = 'Yes'
                break
print(answer)

