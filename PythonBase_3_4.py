import requests
api_url = 'http://numbersapi.com/{}/math'
fileName = input('Enter filename: ')
with open(fileName, 'r') as f:
    for line in f.readlines():
        line = line.strip()
        res = requests.get(api_url.format(line), json=True)
        print('Interesting' if res.json()['found'] else 'Boring')

