import requests, json
client_id = 'c85af18f9db96dc084ea'
client_secret = '2f9194018d9ddcb0597d5b456c6f119a'
apiUrl = 'https://api.artsy.net/api/tokens/xapp_token'
r = requests.post(apiUrl, data={
    'client_id': client_id,
    'client_secret': client_secret
})
j = json.loads(r.text)
token = j['token']
headers = {"X-Xapp-Token" : token}
req_url = "https://api.artsy.net/api/artists/{}"
artists = []
with open('dataset_24476_4 (3).txt', 'rt') as f:
    for line in f.readlines():
        line = line.strip()
        r = requests.get(req_url.format(line), headers=headers).content.decode(encoding='utf-8')
        j = json.loads(r)
        artists.append((j['birthday'], j['sortable_name']))
artists = sorted(artists)
for artist in artists:
    print(artist[1])


