
import xml.etree.ElementTree as etree
tree = etree.fromstring(input())
color_wei = {'blue': 0, 'red': 0, 'green': 0}
def color_weight(root, weight):
    color_wei[root.attrib['color']] += weight
    children = root.getchildren()
    for child in children:
        color_weight(child, weight + 1)
    pass

color_weight(tree, 1)
print('{} {} {}'.format(color_wei['red'], color_wei['green'], color_wei['blue']))